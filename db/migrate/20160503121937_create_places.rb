class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.float :latitude
      t.float :longitude
      t.string :name
      t.integer :rate
      t.string :address
      t.float :couver
      t.string :paymentMethods
      t.string :averageValue
      t.string :operatingDateTime
      t.string :agendaDescription
      t.integer :weeklyViews
      t.integer :monthlyViews
      t.references :creditCard, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
