class CreateCreditCards < ActiveRecord::Migration
  def change
    create_table :credit_cards do |t|
      t.string :cardNumber
      t.integer :expirationMonth
      t.integer :expirationYear
      t.integer :securityCode
      t.string :ownerName
      t.string :creditCardFlag

      t.timestamps null: false
    end
  end
end
