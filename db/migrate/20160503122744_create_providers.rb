class CreateProviders < ActiveRecord::Migration
  def change
    create_table :providers do |t|
      t.float :latitude
      t.float :longitude
      t.string :name
      t.integer :age
      t.integer :rate
      t.string :address
      t.string :paymentMethods
      t.string :value
      t.string :attendedPlaces
      t.string :operatingDateTime
      t.string :description
      t.references :creditCard, index: true, foreign_key: true
      t.integer :weeklyViews
      t.integer :monthlyViews
      t.integer :weeklyContacts
      t.integer :monthlyContacts

      t.timestamps null: false
    end
  end
end
