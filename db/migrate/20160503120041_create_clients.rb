class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :facebookOauth
      t.string :googleOauth
      t.string :user
      t.string :password
      t.string :name
      t.string :profileImage

      t.timestamps null: false
    end
  end
end
