require 'test_helper'

class PlacesControllerTest < ActionController::TestCase
  setup do
    @place = places(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:places)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create place" do
    assert_difference('Place.count') do
      post :create, place: { address: @place.address, agendaDescription: @place.agendaDescription, averageValue: @place.averageValue, couver: @place.couver, creditCard_id: @place.creditCard_id, latitude: @place.latitude, longitude: @place.longitude, monthlyViews: @place.monthlyViews, name: @place.name, operatingDateTime: @place.operatingDateTime, paymentMethods: @place.paymentMethods, rate: @place.rate, weeklyViews: @place.weeklyViews }
    end

    assert_redirected_to place_path(assigns(:place))
  end

  test "should show place" do
    get :show, id: @place
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @place
    assert_response :success
  end

  test "should update place" do
    patch :update, id: @place, place: { address: @place.address, agendaDescription: @place.agendaDescription, averageValue: @place.averageValue, couver: @place.couver, creditCard_id: @place.creditCard_id, latitude: @place.latitude, longitude: @place.longitude, monthlyViews: @place.monthlyViews, name: @place.name, operatingDateTime: @place.operatingDateTime, paymentMethods: @place.paymentMethods, rate: @place.rate, weeklyViews: @place.weeklyViews }
    assert_redirected_to place_path(assigns(:place))
  end

  test "should destroy place" do
    assert_difference('Place.count', -1) do
      delete :destroy, id: @place
    end

    assert_redirected_to places_path
  end
end
