json.array!(@places) do |place|
  json.extract! place, :id, :latitude, :longitude, :name, :rate, :address, :couver, :paymentMethods, :averageValue, :operatingDateTime, :agendaDescription, :weeklyViews, :monthlyViews, :creditCard_id
  json.url place_url(place, format: :json)
end
