json.array!(@credit_cards) do |credit_card|
  json.extract! credit_card, :id, :cardNumber, :expirationMonth, :expirationYear, :securityCode, :ownerName, :creditCardFlag
  json.url credit_card_url(credit_card, format: :json)
end
