json.array!(@providers) do |provider|
  json.extract! provider, :id, :latitude, :longitude, :name, :age, :rate, :address, :paymentMethods, :value, :attendedPlaces, :operatingDateTime, :description, :creditCard_id, :weeklyViews, :monthlyViews, :weeklyContacts, :monthlyContacts
  json.url provider_url(provider, format: :json)
end
