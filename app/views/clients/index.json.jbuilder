json.array!(@clients) do |client|
  json.extract! client, :id, :facebookOauth, :googleOauth, :user, :password, :name, :profileImage
  json.url client_url(client, format: :json)
end
